#!/bin/bash

echo "root-init.sh => install packages"
apt-get -qq update
apt-get install -y build-essential
apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
apt-get install -y python-dev libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
apt-get install -y libv4l-dev libxvidcore-dev libx264-dev libgtk-3-dev libatlas-base-dev gfortran python3.5-dev python2.7-dev
apt-get install -y python3-pip
apt-get install -y sudo wget
# apt-get install -y python-numpy

echo "root-init.sh => Create user opencv, grant privileges and define password"
groupadd -r -g 10001 opencv
useradd -r -u 10001 -g 10001 -m -c "opencv" opencv
adduser opencv sudo
adduser opencv video
echo "opencv:opencv" | chpasswd

echo "opencv ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers

echo "root-init.sh => Create directory /volumes/video and set groups and  user"
mkdir -p /volumes/video
chgrp opencv /volumes
chmod g+rwx /volumes
chgrp opencv /volumes/video
chmod g+rwx /volumes/video


