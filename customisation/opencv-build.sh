#!/bin/bash
echo "opencv-build.sh => Configuring PYTHON ..."

mkdir -p ~/py-build
cd ~/py-build
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py

# echo "build-opencv.sh => Getting virtualenv ..."

sudo pip install virtualenv virtualenvwrapper
sudo rm -rf ~/.cache/pip

# echo "build-opencv.sh => Updating .bashrc ..."
echo -e "\n# Virtualenv and viertualenvwrapper"
echo "export WORKON_HOME=~/.virtualenvs" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc

source ~/.bashrc

export WORKON_HOME=~/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

mkvirtualenv cv -p python3
workon cv

echo "opencv-build.sh => Getting py libraries (pip) ..."

pip install numpy

echo "opencv-build.sh => Getting opencv ..."

mkdir -p ~/opencv-build
cd ~/opencv-build
git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git

echo "opencv-build.sh => Building opencv ..."

workon cv

cd opencv
mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=Release \
      -D CMAKE_INSTALL_PREFIX=/usr/local \
      -D INSTALL_PYTHON_EXAMPLES=ON \
      -D INSTALL_C_EXAMPLES=OFF \
      -D OPENCV_EXTRA_MODULES_PATH=~/opencv-build/opencv_contrib/modules \
      -D PYTHON_EXECUTABLE=~/.virtualenvs/cv/bin/python \
      -D BUILD_EXAMPLES=ON ..

make -j4
# make

echo "opencv-build.sh => Installing opencv ..."

cd ~/opencv-build/opencv/build
sudo make install
sudo ldconfig

cd /usr/local/lib/python3.5/site-packages
sudo mv cv2.cpython-35m-x86_64-linux-gnu.so cv2.so

cd ~/.virtualenvs/cv/lib/python3.5/site-packages/
ln -s /usr/local/lib/python3.5/site-packages/cv2.so cv2.so

#echo "opencv-build.sh => Cleanup opencv sources"

#rm -rf /opt/opencv-build

echo "opencv-build.sh => Finished."