import numpy
import cv2

cap= cv2.VideoCapture(0)

while (True):
    ret, frame= cap.read()
    frame= cv2.resize( frame, (640,480))
    cv2.imshow('test', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
