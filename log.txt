Sending build context to Docker daemon 569.3 kB
Step 1/10 : FROM lauerbt/opencv1
 ---> d0213920cb64
Step 2/10 : MAINTAINER thomas@lauerbach.de
 ---> Using cache
 ---> ed387276c3c6
Step 3/10 : ENV CONTAINER_VERSION 1.0
 ---> Using cache
 ---> fe943a6fd77c
Step 4/10 : ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/lib
 ---> Using cache
 ---> 44613714038b
Step 5/10 : ADD customisation /opt/customisation/
 ---> 6f3544e05fc9
Removing intermediate container 8bde3791c470
Step 6/10 : ADD deployments/test /home/opencv/test
 ---> b861a2d2fe6c
Removing intermediate container fc1719ba875b
Step 7/10 : USER opencv
 ---> Running in d2d85931be4a
 ---> cf80f0bb4e02
Removing intermediate container d2d85931be4a
Step 8/10 : RUN /opt/customisation/opencv-build.sh
 ---> Running in 0b6191eddc8a
opencv-build.sh => Configuring PYTHON ...
[91m--2017-06-23 10:35:34--  https://bootstrap.pypa.io/get-pip.py
[0m[91mResolving bootstrap.pypa.io (bootstrap.pypa.io)... [0m[91m151.101.0.175, 151.101.64.175, 151.101.128.175, ...
Connecting to bootstrap.pypa.io (bootstrap.pypa.io)|151.101.0.175|:443... [0m[91mconnected.
[0m[91mHTTP request sent, awaiting response... [0m[91m200 OK
Length: 1595408 (1.5M) [text/x-python]
[0m[91mSaving to: 'get-pip.py'

     0K .......[0m[91m...[0m[91m ..[0m[91m......[0m[91m..[0m[91m ...[0m[91m...[0m[91m..[0m[91m.. .[0m[91m......... .......[0m[91m...  3% 2.12M 1s
    50K .......... ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m.....  6% 4.54M 0s
   100K .......... .[0m[91m......... .......[0m[91m... .......... ...[0m[91m.......  9% 4.49M 0s
   150K .........[0m[91m. .......... .....[0m[91m..... .......... .[0m[91m......... 12% 5.01M 0s
   200K .......[0m[91m... ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... 16% 3.25M 0s
   250K .......... .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... 19% 5.63M 0s
   300K .........[0m[91m. .......... .....[0m[91m..... .......... .[0m[91m......... 22% 4.49M 0s
   350K .......[0m[91m... .......... ...[0m[91m....... .........[0m[91m. .......... 25% 6.50M 0s
   400K .....[0m[91m..... .......... .[0m[91m......... .......[0m[91m... .......... 28% 5.43M 0s
   450K ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... .......... 32% 7.72M 0s
   500K .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... .........[0m[91m. 35% 4.49M 0s
   550K .......... .....[0m[91m..... .......... .[0m[91m......... .......[0m[91m... 38% 9.19M 0s
   600K .......... ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... 41% 3.13M 0s
   650K .......... .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... 44% 7.03M 0s
   700K .........[0m[91m. .......... .....[0m[91m..... .......... .[0m[91m......... 48% 6.65M 0s
   750K .......[0m[91m... .......... ...[0m[91m....... .........[0m[91m. .......... 51% 7.38M 0s
   800K .....[0m[91m..... .......... .[0m[91m......... .......[0m[91m... .......... 54% 8.34M 0s
   850K ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... .......... 57% 8.50M 0s
   900K .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... .........[0m[91m. 60% 6.58M 0s
   950K .......... .....[0m[91m..... .......... .[0m[91m......... .......[0m[91m... 64% 4.26M 0s
  1000K .......... ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... 67% 11.3M 0s
  1050K .......... .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... 70% 10.9M 0s
  1100K .........[0m[91m. .......... .....[0m[91m..... .......... .[0m[91m......... 73% 6.83M 0s
  1150K .......[0m[91m... .....[0m[91m..... ...[0m[91m....... .........[0m[91m. .......... 77% 10.0M 0s
  1200K .....[0m[91m..... .......... .[0m[91m......... .......[0m[91m... .......... 80% 9.61M 0s
  1250K ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... .......... 83% 8.04M 0s
  1300K .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... .........[0m[91m. 86% 9.35M 0s
  1350K .......... .....[0m[91m..... .......... .[0m[91m......... .......[0m[91m... 89% 7.92M 0s
  1400K .......... ...[0m[91m....... .........[0m[91m. .......... .....[0m[91m..... 93% 11.4M 0s
  1450K ...[0m[91m....... .[0m[91m......... .......[0m[91m... .......... ...[0m[91m....... 96% 11.1M 0s
  1500K .........[0m[91m. .......... .....[0m[91m..... .......... .[0m[91m......... 99% 11.9M 0s
  1550K .......[0m[91m.                           [0m[91m                   100%  294M[0m[91m=0.3s

2017-06-23 10:35:35 (6.06 MB/s) - 'get-pip.py' saved [1595408/1595408]

[0mCollecting pip
  Downloading pip-9.0.1-py2.py3-none-any.whl (1.3MB)
Collecting setuptools
  Downloading setuptools-36.0.1-py2.py3-none-any.whl (476kB)
Collecting wheel
  Downloading wheel-0.29.0-py2.py3-none-any.whl (66kB)
Installing collected packages: pip, setuptools, wheel
Successfully installed pip-9.0.1 setuptools-36.0.1 wheel-0.29.0
Collecting virtualenv
  Downloading virtualenv-15.1.0-py2.py3-none-any.whl (1.8MB)
Collecting virtualenvwrapper
  Downloading virtualenvwrapper-4.7.2.tar.gz (90kB)
Collecting virtualenv-clone (from virtualenvwrapper)
  Downloading virtualenv-clone-0.2.6.tar.gz
Collecting stevedore (from virtualenvwrapper)
  Downloading stevedore-1.23.0-py2.py3-none-any.whl
Collecting pbr!=2.1.0,>=2.0.0 (from stevedore->virtualenvwrapper)
  Downloading pbr-3.1.1-py2.py3-none-any.whl (99kB)
Collecting six>=1.9.0 (from stevedore->virtualenvwrapper)
  Downloading six-1.10.0-py2.py3-none-any.whl
Building wheels for collected packages: virtualenvwrapper, virtualenv-clone
  Running setup.py bdist_wheel for virtualenvwrapper: started
  Running setup.py bdist_wheel for virtualenvwrapper: finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/3e/7e/eb/31f2187dde819aa4f67ade0ac8401da47257f44c213f153ca2
  Running setup.py bdist_wheel for virtualenv-clone: started
  Running setup.py bdist_wheel for virtualenv-clone: finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/24/51/ef/93120d304d240b4b6c2066454250a1626e04f73d34417b956d
Successfully built virtualenvwrapper virtualenv-clone
Installing collected packages: virtualenv, virtualenv-clone, pbr, six, stevedore, virtualenvwrapper
Successfully installed pbr-3.1.1 six-1.10.0 stevedore-1.23.0 virtualenv-15.1.0 virtualenv-clone-0.2.6 virtualenvwrapper-4.7.2

# Virtualenv and viertualenvwrapper
[91m/opt/customisation/opencv-build.sh: line 24: mkvirtualenv: command not found
[0m[91m/opt/customisation/opencv-build.sh: line 25: workon: command not found
[0mopencv-build.sh => Getting py libraries (pip) ...
Requirement already satisfied: numpy in /usr/lib/python2.7/dist-packages
opencv-build.sh => Getting opencv ...
[91mCloning into 'opencv'...
[0m